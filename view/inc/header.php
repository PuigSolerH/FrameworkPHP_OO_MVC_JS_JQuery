<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>FootInfo | <?php if($_GET['module']){echo $_GET['module'];}else{echo "homepage";}?></title>

    <!-- Bootstrap -->
  <link href="<?php echo CSS_PATH?>bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo CSS_PATH?>animate.css">
	<link rel="stylesheet" href="<?php echo CSS_PATH?>font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo CSS_PATH?>jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>normalize.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>demo.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo CSS_PATH?>set1.css" />
	<link href="<?php echo CSS_PATH?>overwrite.css" rel="stylesheet">
	<link href="<?php echo CSS_PATH?>style.css" rel="stylesheet">
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>

  <!--JS-->
  <script type="text/javascript" src="<?php echo JS_PATH ?>main.js"></script>

  <!--UTILS JS-->
  <script type="text/javascript" src="<?php echo JS_PATH?>cookies.js"></script>
  <script type="text/javascript" src="<?php echo LOGIN_JS_PATH?>init.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
