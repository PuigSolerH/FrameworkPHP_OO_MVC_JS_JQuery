<?php
  require_once("paths.php");
?>

<div id="alertbanner"></div>

<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php amigable('?module=main&function=main_view'); ?>"><span>eNno</span></a>
    </div>
    <div class="navbar-collapse collapse">
      <div class="menu">
        <ul class="nav nav-tabs" role="tablist">
          <li class=<?php if((isset($_GET['module'])) && $_GET['module']=='players'){echo 'active';}else{ echo 'deactivate';}?>>
            <a href="<?php amigable('?module=players&function=form_players'); ?>">CREATE</a></li>
          <li class=<?PHP if((isset($_GET['module'])) && $_GET['module']=='home'){echo 'active';}else{ echo 'deactivate';}?>>
            <a href="<?php amigable('?module=home&function=list_players'); ?>">HOME</a></li>
          <li class=<?php if((isset($_GET['module'])) && $_GET['module']=='contact'){echo 'active';}else{ echo 'deactivate';}?>>
            <a href="<?php amigable('?module=contact&function=view_contact'); ?>">CONTACT</a></li>
          <li id="menu_enno"></li>
          <li id="login_menu" class=<?php if((isset($_GET['module'])) && $_GET['module']=='login'){echo 'active';}else{ echo 'deactivate';}?>>
          <a href="<?php amigable('?module=login&function=login'); ?>">LOGIN</a></li>
        </ul>
      </div>
    </div>
  </div>
</nav>
<br/>
<section id="title" class="emerald">
    <div class="container">
        <div class="row">
             <h2 class="BackHome"><a href="<?php amigable('?module=main&function=main_view'); ?>">Back Home</a></h2>
        </div>
    </div>
</section>
