<?php
class main_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = main_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function load_more_data($criteria) {
        return $this->bll->load_more_BLL($criteria);
    }

    public function count_categories() {
        return $this->bll->count_categories_BLL();
    }
}
