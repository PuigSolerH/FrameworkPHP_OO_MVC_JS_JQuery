<?php
  class main_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function load_more_DAO($db, $criteria) {
        $sql = "SELECT id,name,message FROM paginate ORDER BY id ASC LIMIT " . $criteria;
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function count_categories_DAO($db) {
        $sql = "SELECT COUNT(*) FROM paginate";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }
}
