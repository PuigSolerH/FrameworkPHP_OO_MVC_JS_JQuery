<?php
//
//
class main_bll {
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = main_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function load_more_BLL($criteria) {
        return $this->dao->load_more_DAO($this->db,$criteria);
    }

    public function count_categories_BLL() {
        return $this->dao->count_categories_DAO($this->db);
    }
}
