<?php
class controller_main {
    function __construct() {
        include(FUNCTIONS_HOME . "utils.inc.php");
        $_SESSION['module'] = "main";
    }

    function main_view() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView(MAIN_VIEW_PATH, 'main.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function load_more() {
      if (isset($_POST["load_more"])) {
        $result = filter_num_int($_POST["load_more"]);
        // echo json_encode($result);
        // die();
        if ($result['resultado']) {
            $criteria = $result['datos'];
        } else {
            $criteria = 3;
        }
          // echo json_encode($criteria);
          // die();
        set_error_handler('ErrorHandler');
        try {
            $main = loadModel(MODEL_MAIN, "main_model", "load_more_data", $criteria);
        } catch (Exception $e) {
            showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
        }
        restore_error_handler();

        if ($main) {
            $jsondata["load"] = $main;
            echo json_encode($jsondata);
            exit();
        } else {
            showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
        }
      }
    }

    function count_categories() {
      if ($_POST["count_categories"]) {
          $item_per_page = 3;

          if (isset($_POST["categories"])) {
            // echo json_encode($_POST);
            // die();
              $result = filter_string($_POST["categories"]);
              // echo json_encode($result);
              // die();
              if ($result['resultado']) {
                  $criteria = $result['datos'];
              } else {
                  $criteria = '';
              }
          } else {
              $criteria = '';
          }
          // echo json_encode($_POST);
          // die();
          set_error_handler('ErrorHandler');
          try {
              $arrValue = loadModel(MODEL_MAIN, "main_model", "count_categories");//COUNT(*) 75
              $get_total_rows = $arrValue[0]["COUNT(*)"];//(75) //total records
              $pages = ceil($get_total_rows / $item_per_page);//(75/3=25) //break total records into pages
          } catch (Exception $e) {
              showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
          }
          restore_error_handler();

          if ($get_total_rows) {
              $jsondata["categories"] = $pages;
              echo json_encode($jsondata);
              exit;
          } else {
              showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
          }
      }
    }
}
