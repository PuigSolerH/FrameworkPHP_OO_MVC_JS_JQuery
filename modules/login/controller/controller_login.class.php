<?php
class controller_login {
  function __construct() {
      require_once(UTILS_LOGIN . "functions.inc.php");
      // include(LIBS . 'password_compat-master/lib/password.php');
      include(UTILS . 'upload.php');
      $_SESSION['module'] = "login";
      // require_once(LIBS . 'twitteroauth/twitteroauth.php');
  }

  function login() {
      require_once(VIEW_PATH_INC . "header.php");
      require_once(VIEW_PATH_INC . "menu.php");

      loadView(LOGIN_VIEW_PATH . 'login.html');

      require_once(VIEW_PATH_INC . "footer.html");
  }

  function signup() {
      require_once(VIEW_PATH_INC . "header.php");
      require_once(VIEW_PATH_INC . "menu.php");

      loadView(LOGIN_VIEW_PATH . 'signup.html');

      require_once(VIEW_PATH_INC . "footer.html");
  }

  function recover() {
      require_once(VIEW_PATH_INC . "header.php");
      require_once(VIEW_PATH_INC . "menu.php");

      loadView(LOGIN_VIEW_PATH . 'recover.html');

      require_once(VIEW_PATH_INC . "footer.html");
  }

  function profile() {
      require_once(VIEW_PATH_INC . "header.php");
      require_once(VIEW_PATH_INC . "menu.php");

      loadView(LOGIN_VIEW_PATH, 'profile.html');

      require_once(VIEW_PATH_INC . "footer.html");
  }

  function change_pass(){
    require_once(VIEW_PATH_INC . "header.php");
    require_once(VIEW_PATH_INC . "menu.php");

    loadView(LOGIN_VIEW_PATH, 'change.html');

    require_once(VIEW_PATH_INC . "footer.html");
  }

  function login_sn(){
    $user = json_decode($_POST['user'], true);

    set_error_handler('ErrorHandler');
    try {
        $arrArgument = array(
            'usuario' => $user['id']
        );

        $arrValue = loadModel(MODEL_LOGIN, "login_model", "count_SN", $arrArgument);
    } catch (Exception $e) {
        $arrValue = false;
    }
    restore_error_handler();
    // echo json_encode($arrValue[0]["COUNT(*)"]);
    // die();

    if (!$arrValue[0]["COUNT(*)"]) {

        $arrArgument = array(
            'usuario' => $user['id'],
            'nombre' => $user['nombre'],
            'email' => $user['email'],
            'avatar' => $user['userpic'],
            // 'avatar' => $avatar,
            'tipo' => 'client',
            'activado' => "1"
        );
        // echo json_encode($arrArgument);
        // die();

        set_error_handler('ErrorHandler');
        try {
            $value = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);
        } catch (Exception $e) {
            $value = false;
        }
        restore_error_handler();
    } else
        $value = true;

    if ($value) {
        set_error_handler('ErrorHandler');
        $arrArgument = array(
            'like' => array($user['id'])
        );
        $user = loadModel(MODEL_LOGIN, "login_model", "select", $arrArgument);
        restore_error_handler();
        echo json_encode($user);
    } else {
        echo json_encode(array('error' => true, 'datos' => 503));
    }
  }

  function signup_user(){
    // echo json_encode("hola");
    $jsondata = array();
    $userJSON = json_decode($_POST['signup_user_json'], true);

    $result = validate_userPHP($userJSON);
    // echo json_encode($result);
    // die();
    if ($result['resultado']) {
      // echo json_encode("HOLA");
      // die();
        $avatar = get_gravatar($result['datos']['email'], $s = 400, $d = 'identicon', $r = 'g', $img = false, $atts = array());
        $arrArgument = array(
            'usuario' => $result['datos']['username'],
            'email' => $result['datos']['email'],
            'password' => password_hash($result['datos']['password'], PASSWORD_BCRYPT),
            'avatar' => $avatar,
            'tipo' => 'client',
            'token' => ""
        );
        // echo json_encode($arrArgument);
        // die();
        set_error_handler('ErrorHandler');
        try {
          $arrValue = loadModel(MODEL_LOGIN, "login_model", "count", $arrArgument);
          // echo json_encode($arrValue);
          // die();
          if ($arrValue[0]['COUNT(*)'] > 0) {
              $arrValue = false;
              $typeErr = 'Name';
              $error = "Nombre de usuario no disponible";

          } else {
            // echo json_encode($arrArgument);
            // die();
              $arrValue = loadModel(MODEL_LOGIN, "login_model", "count_email", $arrArgument);
              // echo json_encode($arrValue[0]['COUNT(*)']);
              // die();
              if ($arrValue[0]['COUNT(*)'] > 0) {
                  $arrValue = false;
                  $typeErr = 'Email';
                  $error = "Email ya registrado";
              }
            }
        } catch (Exception $e) {
            $arrValue = false;
        }

        if ($arrValue) {
            set_error_handler('ErrorHandler');
            try {
                //loadModel
                $arrArgument['token'] = "Ver" . md5(uniqid(rand(), true));
                // echo json_encode($arrArgument);
                // die();
                $arrValue = loadModel(MODEL_LOGIN, "login_model", "create_user", $arrArgument);
                // DELETE FROM users WHERE activado = '0'
                // echo json_encode($arrValue);
                // die();
            } catch (Exception $e) {
                $arrValue = false;
            }
            restore_error_handler();

            if ($arrValue) {
                sendtoken($arrArgument, "alta");

                $url = amigable('?module=main&function=main_view', true);
                $jsondata["success"] = true;
                $jsondata["redirect"] = $url;
                echo json_encode($jsondata);
                exit;
            } else {
                $url = amigable('?module=main&function=main_view&param=503', true);
                $jsondata["success"] = true;
                $jsondata["redirect"] = $url;
                echo json_encode($jsondata);
            }
        } else {
            $jsondata["success"] = false;
            $jsondata['typeErr'] = $typeErr;
            $jsondata["error"] = $error;
            echo json_encode($jsondata);
        }
        restore_error_handler();
    } else {
        $jsondata["success"] = false;
        $jsondata['datos'] = $result;
        echo json_encode($jsondata);
    }
  }

  function verify() {
    // echo json_encode($_GET);
    // die();
      if (substr($_GET['aux'], 0, 3) == "Ver") {
        // echo json_encode($_GET);
        // die();
          $arrArgument = array(
              'token' => $_GET['aux']
          );
// echo json_encode($arrArgument);
// die();
          set_error_handler('ErrorHandler');
          try {
              $value = loadModel(MODEL_LOGIN, "login_model", "verify", $arrArgument);
              // echo json_encode($value);
              // die();
          } catch (Exception $e) {
              $value = false;
          }
          restore_error_handler();

          if ($value) {
            require_once(VIEW_PATH_INC . "header.php");
            require_once(VIEW_PATH_INC . "menu.php");
            loadView(MAIN_VIEW_PATH . 'main.html');
            require_once(VIEW_PATH_INC . "footer.html");

          } else {
              showErrorPage(1, "", 'HTTP/1.0 503 Service Unavailable', 503);
          }
      }
  }

  function login_manual(){
      $user = json_decode($_POST['login_json'], true);

      $arrArgument = array(
          'username' => $user['username'],
          'password' => $user['pass']
      );

      set_error_handler('ErrorHandler');
      try {
          $arrValue = loadModel(MODEL_LOGIN, "login_model", "select_manual", $arrArgument);
          // echo json_encode($arrValue);
          // die();
          $arrValue = password_verify($user['pass'], $arrValue[0]['password']);
          // echo json_encode($arrValue);
          // die();
      } catch (Exception $e) {
          $arrValue = false;
      }
      restore_error_handler();

      if ($arrValue !== "error") {
          if ($arrValue) { //OK
              set_error_handler('ErrorHandler');
              try {
                  $arrArgument = array(
                      'usuario' => $user['username'],
                      'activado' => "1"
                  );
                  // echo json_encode($arrArgument['like'][0]);
                  // die();
                  $arrValue = loadModel(MODEL_LOGIN, "login_model", "count_activado", $arrArgument);
                  // echo json_encode($arrValue);
                  // exit();
                  if ($arrValue[0]["COUNT(*)"] == 1) {
                      $arrArgument = array(
                          'column' => array("usuario"),
                          'like' => array($user['username'])
                      );
                      // echo json_encode($arrArgument);
                      // die();
                      $user = loadModel(MODEL_LOGIN, "login_model", "select", $arrArgument);

                      echo json_encode($user);
                      exit();
                  } else {
                      $value = array(
                          "error" => true,
                          "datos" => "El usuario no ha sido activado, revise su correo"
                      );
                      echo json_encode($value);
                      exit();
                  }
              } catch (Exception $e) {
                  $value = array(
                      "error" => true,
                      "datos" => 503
                  );
                  echo json_encode($value);
              }
          } else {
              $value = array(
                  "error" => true,
                  "datos" => "El usuario y la contraseña no coinciden"
              );
              echo json_encode($value);
          }
      } else {
          $value = array(
              "error" => true,
              "datos" => 503
          );
          echo json_encode($value);
      }
  }

// Recover
  function restore(){
      $result = array();
      if (isset($_POST['r_email'])) {
          $result = validatemail($_POST['r_email']);
          if ($result) {

              $token = "Cha" . md5(uniqid(rand(), true));

              $arrArgument = array(
                  'email' => $_POST['r_email'],
                  'token' => $token
              );
              $arrValue = loadModel(MODEL_LOGIN, "login_model", "count_email_pass", $arrArgument);
              // echo json_encode($arrValue);
              // die();
              if ($arrValue[0]['COUNT(*)'] == 1) {
                  $arrValue = loadModel(MODEL_LOGIN, "login_model", "update_token", $arrArgument);
                  // echo json_encode($arrValue);
                  // die();
                  if ($arrValue) {
                      //////////////// Envio del correo al usuario
                      $arrArgument = array(
                          'token' => $token,
                          'email' => $_POST['r_email']
                      );
                      if (sendtoken($arrArgument, "modificacion"))
                          echo "Tu nueva contraseña ha sido enviada al email";
                      else
                          echo "Error en el servidor. Intentelo más tarde";
                  }
              } else {
                  echo "El email introducido no existe ";
              }
          } else {
              echo "El email no es válido";
          }
      }
  }

  function update_pass() {
    // echo json_encode($_POST);
    // die();
      $jsondata = array();
      $pass = json_decode($_POST['password'], true);
      // echo json_encode($pass);
      // die();

      $arrArgument = array(
        'password' => password_hash($pass['password'], PASSWORD_BCRYPT),
        'token' => $pass['token']
      );
      // echo json_encode($arrArgument);
      // die();

      set_error_handler('ErrorHandler');
      try {
          $value = loadModel(MODEL_LOGIN, "login_model", "update_password", $arrArgument);
      } catch (Exception $e) {
          $value = false;
      }
      restore_error_handler();

      if ($value) {
          $url = amigable('?module=main&function=main_view', true);
          $jsondata["success"] = true;
          $jsondata["redirect"] = $url;
          echo json_encode($jsondata);
          exit;
      } else {
          $url = amigable('?module=main&function=main_view', true);
          $jsondata["success"] = true;
          $jsondata["redirect"] = $url;
          echo json_encode($jsondata);
          exit;
      }
  }

//profile
  function upload_avatar() {
      $result_avatar = upload_files();
      $_SESSION['avatar'] = $result_avatar;
  }

  function delete_avatar() {
      $_SESSION['avatar'] = array();
      $result = remove_files();
      if ($result === true) {
          echo json_encode(array("res" => true));
      } else {
          echo json_encode(array("res" => false));
      }
  }

  function profile_filler() {
    // echo json_encode($_POST);
    // die();
      if (isset($_POST['usuario'])) {
          set_error_handler('ErrorHandler');
          try {
              $arrArgument = array(
                  'username' => $_POST['usuario'],
              );
              // echo json_encode($arrArgument);
              // die();
              $arrValue = loadModel(MODEL_LOGIN, "login_model", "select_manual", $arrArgument);
              // echo json_encode($arrValue);
              // die();
          } catch (Exception $e) {
              $arrValue = false;
          }
          restore_error_handler();

          if ($arrValue) {
              $jsondata["success"] = true;
              $jsondata['user'] = $arrValue[0];
              echo json_encode($jsondata);
              exit();
          } else {
              $url = amigable('?module=main', true);
              $jsondata["success"] = false;
              $jsondata['redirect'] = $url;
              echo json_encode($jsondata);
              exit();
          }
      } else {
          $url = amigable('?module=main&function=main_view', true);
          $jsondata["success"] = false;
          $jsondata['redirect'] = $url;
          echo json_encode($jsondata);
          exit();
      }
  }

  function update_profile() {
    // echo json_encode($_POST);
    // die();
      $jsondata = array();
      $userJSON = json_decode($_POST['profile_user'], true);
      // echo json_encode($userJSON);
      // echo json_encode($_SESSION);
      // die();
      // $userJSON['password2'] = $userJSON['password'];

      $result = validate_userPHP($userJSON);
      // echo json_encode($result['datos']);
      // die();
      if ($result['resultado']) {
          $arrArgument = array(
              'name' => $userJSON['name'],
              'username' => $result['datos']['username'],
              'email' => $result['datos']['email'],
              'password' => password_hash($result['datos']['password'], PASSWORD_BCRYPT),
              'date_birthday' => strtoupper($userJSON['date_birthday']),
              'avatar' => $_SESSION['avatar']['data']
          );
          // echo json_encode($arrArgument);
          // die();
          set_error_handler('ErrorHandler');
          try {
              $arrValue = loadModel(MODEL_LOGIN, "login_model", "update_profile", $arrArgument);
          } catch (Exception $e) {
              $arrValue = false;
          }
          restore_error_handler();
          if ($arrValue) {
              $url = amigable('?module=login&function=profile', true);
              $jsondata["success"] = true;
              $jsondata["redirect"] = $url;
              echo json_encode($jsondata);
              exit;
          } else {
              $jsondata["success"] = false;
              $jsondata["redirect"] = $url = amigable('?module=login&function=profile&aux=503', true);
              echo json_encode($jsondata);
          }
      } else {
          $jsondata["success"] = false;
          $jsondata['datos'] = $result;
          echo json_encode($jsondata);
      }
  }

}
