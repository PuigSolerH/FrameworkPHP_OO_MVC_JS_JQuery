$(document).ready(function () {
  $('#s_signup').click(function () {
      validate_user();
  });

  // $("#s_username, #s_email, #s_password").keyup(function () {
  //     if ($(this).val() !== "") {
  //         $(".error").fadeOut();
  //         return false;
  //     }
  // });

  $("#s_username").keyup(function () {
      if ($(this).val().length >= 3) {
          $(".error").fadeOut();
          return false;
      }
  });
  $("#s_email").keyup(function () {
      var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
      if ($(this).val() !== "" && emailreg.test($(this).val())) {
          $(".error").fadeOut();
          return false;
      }
  });
  $("#s_password").keyup(function () {
      if ($(this).val().length >= 6) {
          $(".error").fadeOut();
          return false;
      }
  });
});

function validate_user() {
    var result = true;
    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    var nomreg = /^\D{3,30}$/;
    var username = $("#s_username").val();
    // console.log(username);
    var email = $("#s_email").val();
    // console.log(email);
    var password = $("#s_password").val();
    // console.log(password);

    $(".error").remove();
    if ($("#s_username").val() === "" || !nomreg.test($("#s_username").val())) {
        $("#s_username").focus().after("<span class='error' style='color:#FF0000';>Usuario no válido</span>");
        result = false;
    } else if ($("#s_username").val().length < 3) {
        $("#s_username").focus().after("<span class='error' style='color:#FF0000';>Mínimo 3 carácteres para el usuario</span>");
        result = false;
    } else if (!emailreg.test($("#s_email").val()) || $("#s_email").val() === "") {
        $("#s_email").focus().after("<span class='error' style='color:#FF0000';>Ingrese un email correcto</span>");
        result = false;
    } else if ($("#s_password").val() === "") {
        $("#s_password").focus().after("<span class='error' style='color:#FF0000';>Ingrese su contraseña</span>");
        result = false;
    } else if ($("#s_password").val().length < 6) {
        $("#s_password").focus().after("<span class='error' style='color:#FF0000';>Mínimo 6 carácteres para la contraseña</span>");
        result = false;
    }

// console.log(result);
    if (result) {
        var data = {"username": username, "email": email, "password": password};
        // console.log(data);
        var data_users_JSON = JSON.stringify(data);
        $.post(amigable("?module=login&function=signup_user"), {signup_user_json: data_users_JSON},
        function (response) {
            console.log(response);
            if (response.success) {
                window.location.href = response.redirect;
            } else {
              // console.log(response["username"]);
                if (response.typeErr === "Name") {
                    $("#s_username").focus().after("<span class='error' style='color:#FF0000'>" + response.error + "</span>");
                } else if (response.typeErr === "Email") {
                    $("#s_email").focus().after("<span class='error' style='color:#FF0000'>" + response.error + "</span>");
                } else {
                    if (response["datos"]["username"] !== undefined && response["datos"]["username"] !== null) {
                        $("#s_username").focus().after("<span class='error' style='color:#FF0000'>" + response["datos"]["username"] + "</span>");
                    }
                    if (response["datos"]["email"] !== undefined && response["datos"]["email"] !== null) {
                        $("#s_email").focus().after("<span class='error' style='color:#FF0000'>" + response["datos"]["email"] + "</span>");
                    }
                    if (response["datos"]["password"] !== undefined && response["datos"]["password"] !== null) {
                        $("#s_password").focus().after("<span class='error' style='color:#FF0000'>" + response.error.password + "</span>");
                    }
                }
            }
        }, "json").fail(function (xhr, textStatus, errorThrown) {
            // console.log(xhr);
            console.log(xhr.responseJSON);
            console.log(xhr.responseText);
            if( (xhr.responseJSON === undefined) || (xhr.responseJSON === null) )
                xhr.responseJSON = JSON.parse(xhr.responseText);
            if (xhr.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (xhr.status === 404) {
                alert('Requested page not found [404]');
            } else if (xhr.status === 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + xhr.responseText);
            }
        });
    }
}
