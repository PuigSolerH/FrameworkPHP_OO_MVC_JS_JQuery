$(document).ready(function () {
  $( "#login" ).click(function() {
    login();
  });
});
  function login(){
    var user = $("#l_username").val();
    var pass = $("#l_password").val();
    var value = false;
    // console.log(user +"|"+ pass);

    $(".error").remove();
    if (!user) {
        $("#l_username").focus().after("<span class='error' style='color:#FF0000';>Usuario vacío</span>");
        value = false;
    } else {
        if (!pass) {
            $("#l_password").focus().after("<span class='error' style='color:#FF0000';>Contraseña vacía</span>");
            value = false;
        } else
            value = true;
    }

    var data = {"username": user, "pass": pass};
    var login = JSON.stringify(data);
    console.log(login);
    if (value){
      $.post(amigable("?module=login&function=login_manual"), {login_json: login},
      function (response){
          console.log(response);
          if (!response.error) {
            Tools.createCookie("user", response[0]['username'] + "|" + response[0]['userpic'] + "|" + response[0]['tipo'] + "|" + response[0]['name'], 1);
            window.location.href = amigable("?module=main&function=main_view");
          } else {
            if (response.datos == 503)
              window.location.href = amigable("?module=main&function=main_view&aux=503");
            else
              $("#l_password").focus().after("<span class='error' style='color:#FF0000';>" + response.datos + "</span>");
          }
      }, "json").fail(function (xhr, textStatus, errorThrown) {
                  // console.log(textStatus);
                  // console.log(errorThrown);
                  console.log(xhr);
                  console.log(xhr.responseText);
                  if (xhr.status === 0) {
                      alert('Not connect: Verify Network.');
                  } else if (xhr.status === 404) {
                      alert('Requested page not found [404]');
                  } else if (xhr.status === 500) {
                      alert('Internal Server Error [500].');
                  } else if (textStatus === 'parsererror') {
                      alert('Requested JSON parse failed.');
                  } else if (textStatus === 'timeout') {
                      alert('Time out error.');
                  } else if (textStatus === 'abort') {
                      alert('Ajax request aborted.');
                  } else {
                      alert('Uncaught Error: ' + xhr.responseText);
                  }
              });
    }
  }
