function paint(dataString) {
    $("#resultMessage").html("<div class='alert alert-success'>"+dataString+"</div>").fadeIn("slow");

    setTimeout(function () {
        $("#resultMessage").fadeOut("slow")
    }, 5000);

    //reset the form
    $('#recover_form')[0].reset();

    // hide ajax loader icon
    $('.ajaxLoader').fadeOut("fast");

    // Enable button after processing
    $('#recover').attr('disabled', false);

    $url = amigable('?module=main&function=main_view');
    setTimeout(function () {
        window.location.href = $url;
    }, 3000);
}

$(document).ready(function () {
  // $( "#recover" ).click(function() {
  //   alert( "RECOVER" );
  // });
  $(function () {
      $('#recover').attr('disabled', false);
  });

  $("#recover_form").validate({
      rules: {
          r_email: {
              required: true,
              email: true
          }
      },
      highlight: function (element) {
          $(element).closest('.form-group').removeClass('success').addClass('errorr');
      },
      success: function (element) {
          $(element).closest('.form-group').removeClass('errorr').addClass('success');
          $(element).closest('.form-group').find('label').remove();
      },
      errorClass: "help-inline"
  });

  $("#recover").click(function () {
    // alert("HOLA");
      if ($("#recover_form").valid()) {
        // alert("HOLA");

          // Disable button while processing
          $('#recover').attr('disabled', true);

          // show ajax loader icon
          $('.ajaxLoader').fadeIn("fast");

          var dataString = $("#recover_form").serialize();
          console.log(dataString);
          $.ajax({
              type: "POST",
              url: amigable("?module=login&function=restore"),
              data: dataString,
              success: function (dataString) {
                alert(dataString);
                  paint(dataString);
              }
          })
          .fail(function () {
              paint("<div class='alert alert-error'>Error en el servidor. Intentelo más tarde...</div>");
          });
      }
      return false;
  });

});
