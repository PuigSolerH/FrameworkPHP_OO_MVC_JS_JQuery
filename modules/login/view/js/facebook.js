$(document).ready(function () {
  var config = {
    apiKey: "AIzaSyB2jVcliAv1DuoU-RjkV842auO-w3u2N4A",
    authDomain: "enno-1daw.firebaseapp.com",
    databaseURL: "https://enno-1daw.firebaseio.com",
    projectId: "enno-1daw",
    storageBucket: "enno-1daw.appspot.com",
    messagingSenderId: "298339565970"
  };
 firebase.initializeApp(config);

 // creo el provider de autenticación
 var provider = new firebase.auth.FacebookAuthProvider();
 // opcionalmente modifico el scope
 //provider.addScope('user_friends');

 // accedo al servicio de autenticación
 var authService = firebase.auth();

 document.getElementById('fb_login').addEventListener('click', function() {
     // autentico con Facebook
     authService.signInWithPopup(provider)
         .then(function(result) {
             // console.log('autenticado usuario ', result.user);
             // console.log(result.user.uid);
             // console.log(result.user.displayName);
             // console.log(result.user.email);
             // console.log(result.user.photoURL);

                 var data = {"id": result.user.uid, "nombre": result.user.displayName, "email": result.user.email, "userpic":result.user.photoURL };
                 var datos_social = JSON.stringify(data);
                 // console.log(datos_social);

                 $.post('../../login/login_sn', {user: datos_social},
                 // $.post(amigable('?module=login&function=login_sn'), {user: datos_social},
                 function (response) {
                   console.log(response);
                     if (!response.error) {
                         Tools.createCookie("user", response[0]['username'] + "|" + response[0]['userpic'] + "|" + response[0]['tipo'] + "|" + response[0]['name'], 1);
                         // window.location.href = "../../main/main_view";
                         window.location.href = amigable("?module=main&function=main_view");
                     } else {
                         if (response.datos == 503)
                             window.location.href = amigable("?module=main&function=begin&aux=503");
                     }
                 }, "json").fail(function (xhr, textStatus, errorThrown) {
                   // console.log(xhr);
                   console.log(textStatus);
                   console.log(errorThrown);
                     console.log(xhr.responseText);
                     if (xhr.status === 0) {
                         alert('Not connect: Verify Network.');
                     } else if (xhr.status === 404) {
                         alert('Requested page not found [404]');
                     } else if (xhr.status === 500) {
                         alert('Internal Server Error [500].');
                     } else if (textStatus === 'parsererror') {
                         alert('Requested JSON parse failed.');
                     } else if (textStatus === 'timeout') {
                         alert('Time out error.');
                     } else if (textStatus === 'abort') {
                         alert('Ajax request aborted.');
                     } else {
                         alert('Uncaught Error: ' + xhr.responseText);
                     }
                 });
             })
         .catch(function(error) {
             console.log('Detectado un error:', error);
         });
 })

 // function Logout() {
 //     FB.logout(function () {
 //         document.location.reload();
 //         Tools.eraseCookie("user");
 //         window.location.href = amigable("?module=main");
 //     });
 // }
});
