$(document).ready(function () {
  $('#profile_btn').click(function () {
      validate_profile();
      // alert("HOLA");
  });

  $("#p_birth").datepicker({
      maxDate: '0',
      changeMonth: true,
      changeYear: true,
      yearRange: "1930:2000"
  });

  $("#p_name, #p_password, #p_username").keyup(function () {
      if ($(this).val() !== "") {
          $(".error").fadeOut();
          return false;
      }
  });
  $("#p_name").keyup(function () {
      if ($(this).val().length >= 2) {
          $(".error").fadeOut();
          return false;
      }
  });
  $("#p_password").keyup(function () {
      if ($(this).val().length >= 6) {
          $(".error").fadeOut();
          return false;
      }
  });
  $("#p_username").keyup(function () {
      if ($(this).val().length >= 3) {
          $(".error").fadeOut();
          return false;
      }
  });


  $("#progress").hide();

  Dropzone.autoDiscover = false;
  $("#dropzone").dropzone({
      url: amigable("?module=login&function=upload_avatar"),
      addRemoveLinks: true,
      maxFileSize: 1000,
      dictResponseError: "Ha ocurrido un error en el server",
      acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
      init: function () {
          this.on("success", function (file, response) {
              $("#progress").show();
              $("#bar").width('100%');
              $("#percent").html('100%');
              $('.msg').text('').removeClass('msg_error');
              $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
          });
      },
      complete: function (file) {
          //if(file.status == "success"){
          //alert("El archivo se ha subido correctamente: " + file.name);
          //}
      },
      error: function (file) {
          //alert("Error subiendo el archivo " + file.name);
      },
      removedfile: function (file, serverFileName) {
          var name = file.name;
          $.ajax({
              type: "GET",
              url: amigable("?module=login&function=delete_avatar&delete=true"),
              data: {"filename": name},
              success: function (data) {
                  $("#progress").hide();
                  $('.msg').text('').removeClass('msg_ok');
                  $('.msg').text('').removeClass('msg_error');
                  $("#p_avatar").html("");

                  var json = JSON.parse(data);
                  if (json.res === true) {
                      var element;
                      if ((element = file.previewElement) != null) {
                          element.parentNode.removeChild(file.previewElement);
                          //alert("Imagen eliminada: " + name);
                      } else {
                          false;
                      }
                  } else { //json.res == false, elimino la imagen también
                      var element;
                      if ((element = file.previewElement) != null) {
                          element.parentNode.removeChild(file.previewElement);
                      } else {
                          false;
                      }
                  }
              }
          });
      }
  });

  var user = Tools.readCookie("user");
  // console.log(user);
  if (user) {
      user = user.split("|");
      console.log(user);
      $.post(amigable('?module=login&function=profile_filler'), {usuario: user[0]},
      function (response) {
        console.log(response);
          if (response.success) {
              fill(response.user);
          } else {
              window.location.href = response.redirect;
          }
      }, "json").fail(function (xhr, textStatus, errorThrown) {
          console.log(xhr.responseText);
          if (xhr.status === 0) {
              alert('Not connect: Verify Network.');
          } else if (xhr.status === 404) {
              alert('Requested page not found [404]');
          } else if (xhr.status === 500) {
              alert('Internal Server Error [500].');
          } else if (textStatus === 'parsererror') {
              alert('Requested JSON parse failed.');
          } else if (textStatus === 'timeout') {
              alert('Time out error.');
          } else if (textStatus === 'abort') {
              alert('Ajax request aborted.');
          } else {
              alert('Uncaught Error: ' + xhr.responseText);
          }
      });
  }else{
      alert('User profile not available');
  }
});

function validate_profile() {
    var result = true;
    var nomreg = /^\D{3,30}$/;
    var apelreg = /^(\D{3,30})+$/;
    var nombre = $("#p_name").val();
    var username = $("#p_username").val();
    var email = $("#p_email").val();
    var password = $("#p_password").val();
    var date_birthday = $("#p_birth").val();

    $(".error").remove();
    if ($("#p_name").val() === "" || !nomreg.test($("#p_name").val())) {
        $("#p_name").focus().after("<span class='error'>Ingrese su nombre</span>");
        result = false;
    } else if ($("#p_name").val().length < 2) {
        $("#p_name").focus().after("<span class='error'>Mínimo 2 carácteres para el nombre</span>");
        result = false;
    } else if ($("#p_username").val() === "" || !apelreg.test($("#p_username").val())) {
        $("#p_username").focus().after("<span class='error'>Ingrese sus username</span>");
        result = false;
    } else if ($("#p_username").val().length < 3) {
        $("#p_username").focus().after("<span class='error'>Mínimo 3 carácteres para los username</span>");
        result = false;
    } else if ($("#p_password").val() === "") {
        $("#p_password").focus().after("<span class='error'>Ingrese su contraseña</span>");
        result = false;
    } else if ($("#p_password").val().length < 6) {
        $("#p_password").focus().after("<span class='error'>Mínimo 6 carácteres para la contraseña</span>");
        result = false;
    }

    if (result) {
        var data = {"name": nombre, "date_birthday": date_birthday, "password": password, "username": username, "email": email};
        var data_users_JSON = JSON.stringify(data);
        console.log(data_users_JSON);
        $.post(amigable('?module=login&function=update_profile'), {profile_user: data_users_JSON},
        function (response) {
          console.log(response);
            if (response.success) {
                window.location.href = response.redirect;
            } else {
                if (response.redirect) {
                    window.location.href = response.redirect;
                } else
                if (response["datos"]["nombre"] !== undefined && response["datos"]["nombre"] !== null) {
                    $("#p_name").focus().after("<span class='error'>" + response["datos"]["nombre"] + "</span>");
                }
                if (response["datos"]["username"] !== undefined && response["datos"]["username"] !== null) {
                    $("#p_username").focus().after("<span class='error'>" + response["datos"]["username"] + "</span>");
                }
                if (response["datos"]["password"] !== undefined && response["datos"]["password"] !== null) {
                    $("#inputPass").focus().after("<span class='error'>" + response.error.password + "</span>");
                }
                if (response["datos"]["date_birthday"] !== undefined && response["datos"]["date_birthday"] !== null) {
                    $("#inputBirth").focus().after("<span class='error'>" + response["datos"]["date_birthday"] + "</span>");
                }
            }
        }, "json").fail(function (xhr, textStatus, errorThrown) {
          console.log(xhr.responseText);
            if (xhr.responseJSON === undefined || xhr.responseJSON === null)
                xhr.responseJSON = JSON.parse(xhr.responseText);
            if (xhr.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (xhr.status === 404) {
                alert('Requested page not found [404]');
            } else if (xhr.status === 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + xhr.responseText);
            }
        });
    }
}

function fill(user) {
  console.log(user);
    $("#p_username").val(user['username']);
    $("#p_name").val(user['name']);
    $("#p_birth").val(user['birth_date']);
    $("#p_password").val("");
    $("#p_avatar_user").attr('src', user['userpic']);
    $("#p_email").val(user['email']);
    if (user['email'])
        $("#p_email").attr('disabled', true);
}
