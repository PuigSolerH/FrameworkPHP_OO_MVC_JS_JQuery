<?php
class controller_players {

    function __construct() {
        include(FUNCTIONS_PLAYERS . "functions_players.inc.php");
        include(UTILS . "upload.php");
        $_SESSION['module'] = "players";
    }

    function form_players() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        // echo '<br><br><br><br><br><br><br>';
        loadView(PLAYERS_VIEW_PATH, 'create_players.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function results_players() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        // echo '<br><br><br><br><br><br><br>';
        loadView(PLAYERS_VIEW_PATH, 'results_players.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function upload_players() {
      $result_avatar = upload_files();
      $_SESSION['avatar'] = $result_avatar;
    }

    function alta_players(){
      // echo json_encode($_SESSION);
      // echo json_encode($_POST);
      // die();
      if ((isset($_POST['alta_players_json']))) {
        $jsondata = array();
        $playersJSON = json_decode($_POST["alta_players_json"], true);
        $result= validate_players($playersJSON);

        if (empty($_SESSION['avatar'])){
            $_SESSION['avatar'] = array('result' => true, 'error' => "", 'data' => "/enno/media/default-avatar.png");
        }
        $img = $_SESSION['avatar'];
        // echo json_encode($img);
        // die();

        if(($result['result']) && ($img['result'])) {
            $arrArgument = array(
              'name' => $result['data']['name'],
              'surname' => $result['data']['surname'],
              'position' => $result['data']['position'],
              'foot' => $result['data']['foot'],
              'country' => $result['data']['country'],
              'league' => $result['data']['league'],
              'team' => $result['data']['team'],
              'date_reception' => $result['data']['date_reception'],
              'date_expiration' => $result['data']['date_expiration'],
              'appearances' => $result['data']['appearances'],
              'description' => $result['data']['description'],
              'img' => $img['data']
            );
            // echo json_encode($arrArgument);
            // die();


            $arrValue = false;
            try {
                  $arrValue = loadModel(MODEL_PLAYERS, "players_model", "create_player", $arrArgument);
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            // echo json_encode($arrValue);
            // die();
            //
            if ($arrValue){
                $message = "Product has been successfull registered";
            }else{
                $message = "Problem ocurred registering a player";
            }
            $_SESSION['player'] = $arrArgument;
            // echo json_encode($_SESSION['player']);
            // die();
            $_SESSION['message'] = $message;
            $callback="../../players/results_players";//index.php?module=players&view=results_players

            $jsondata["success"] = true;
            $jsondata["redirect"] = $callback;
            echo json_encode($jsondata);
            exit;
        }else{
          $jsondata["success"] = false;
          $jsondata["error"] = $result['error'];
          $jsondata["error_avatar"] = $img['error'];

          $jsondata["success1"] = false;
          if ($img['result']) {
              $jsondata["success1"] = true;
              $jsondata["avatar"] = $img['data'];
          }
          header('HTTP/1.0 400 Bad error', true, 404);
          echo json_encode($jsondata);
        }//End else
      }
    }//End alta players

    function delete_players() {
        if (isset($_POST["delete"]) && $_POST["delete"] == true) {
            $_SESSION['avatar'] = array();
            $result = remove_files();
            if ($result === true) {
                echo json_encode(array("res" => true));
            } else {
                echo json_encode(array("res" => false));
            }
        }
    }

    function load_players() {
        if ((isset($_POST["load"])) && ($_POST["load"] == true)) {
            $jsondata = array();
            if (isset($_SESSION['player'])) {
                // echo debug($_SESSION['player']);
                $jsondata["player"] = $_SESSION['player'];
            }
            if (isset($_SESSION['msje'])) {
                // echo $_SESSION['msje'];
                $jsondata["msje"] = $_SESSION['msje'];
            }
            close_session();
            echo json_encode($jsondata);
            exit;
        }
    }

    function load_data_players() {
        if ((isset($_POST["load_data"])) && ($_POST["load_data"] == true)) {
            $jsondata = array();

            if (isset($_SESSION['player'])) {
                $jsondata["player"] = $_SESSION['player'];
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["player"] = "";
                echo json_encode($jsondata);
                exit;
            }
        }
    }
}
