<?php
// echo json_encode("DAO");
// exit();
class players_dao {

    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_player_DAO($db, $arrArgument) {
        $name = $arrArgument['name'];
        $surname = $arrArgument['surname'];
        $position = $arrArgument['position'];
        // $foot = $arrArgument['foot'];
        $foot = "";
        foreach ($arrArgument['foot'] as $indice) {
          // print_r($indice);
          // die();
            $foot=$foot."$indice:";
        }
        $signin = $arrArgument['date_reception'];
        $expiration = $arrArgument['date_expiration'];
        $appearances = $arrArgument['appearances'];
        $country = $arrArgument['country'];
        $league = $arrArgument['league'];
        $team = $arrArgument['team'];
        $description = $arrArgument['description'];
        $img = $arrArgument['img'];

        // $foot1=0;
        // $foot2=0;
        //
        // foreach ($category as $indice) {
        //     if ($indice === 'left')
        //         $foot1 = 1;
        //     if ($indice === 'right')
        //         $foot2 = 1;
        // }

        $sql = "INSERT INTO players(name, surname, position, foot, country, league, team, signin, expiration, appearances, description, img) VALUES ('$name', '$surname','$position', '$foot', '$country', '$league', '$team', '$signin', '$expiration', '$appearances', '$description', '$img')";

        return $db->ejecutar($sql);
    }

    // public function obtain_countries_DAO(){
    //       $json = array();
    //       $tmp = array();
    //
    //       $provincias = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/enno/resources/countries.json');
    //       $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
    //       for ($i=0; $i<count($result); $i+=2) {
    //         $e=$i+1;
    //         $provincia=$result[$e];
    //
    //         $tmp = array(
    //           'id' => (string) $result[$i], 'nombre' => (string) $provincia
    //         );
    //         array_push($json, $tmp);
    //       }
    //           return $json;
    //
    // }

    // public function obtain_countries_DAO($url){
    //       $ch = curl_init();
    //       curl_setopt ($ch, CURLOPT_URL, $url);
    //       curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    //       curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
    //       $file_contents = curl_exec($ch);
    //
    //       $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    //       curl_close($ch);
    //       $accepted_response = array(200, 301, 302);
    //       if(!in_array($httpcode, $accepted_response)){
    //         return FALSE;
    //       }else{
    //         return ($file_contents) ? $file_contents : FALSE;
    //       }
    // }
    //
    // public function obtain_provinces_DAO(){
    //       $json = array();
    //       $tmp = array();
    //
    //       $provincias = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/1_Backend/5_dependent_dropdowns/resources/provinciasypoblaciones.xml');
    //       $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
    //       for ($i=0; $i<count($result); $i+=2) {
    //         $e=$i+1;
    //         $provincia=$result[$e];
    //
    //         $tmp = array(
    //           'id' => (string) $result[$i], 'nombre' => (string) $provincia
    //         );
    //         array_push($json, $tmp);
    //       }
    //           return $json;
    //
    // }
    //
    // public function obtain_cities_DAO($arrArgument){
    //       $json = array();
    //       $tmp = array();
    //
    //       $filter = (string)$arrArgument;
    //       $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/1_Backend/5_dependent_dropdowns/resources/provinciasypoblaciones.xml');
    //       $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");
    //
    //       for ($i=0; $i<count($result[0]); $i++) {
    //           $tmp = array(
    //             'poblacion' => (string) $result[0]->localidad[$i]
    //           );
    //           array_push($json, $tmp);
    //       }
    //       return $json;
    // }
}//End playerDAO
