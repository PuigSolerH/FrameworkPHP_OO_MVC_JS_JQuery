function validate_player(){
    var result = true;

    // var name = document.getElementById('name').value;
    var name = $('#name').val();
    // console.log(name);
    var surname = $('#surname').val();
    var appearances = $('#appearances').val();
    var date_reception = $('#date_reception').val();
    var date_expiration = $('#date_expiration').val();
    var foot = new Array();
    var inputElements = $('.footCheckbox')
    // console.log(inputElements);
    var j=0;
    for (var i=0; i< inputElements.length; i++){
        if (inputElements[i].checked){
          foot[j] = inputElements[i].value;
          j++;
        }
    }
    // console.log(inputElements);
    // console.log(foot);
    var position = $('input[name="position"]:checked').val();
  /*  var position = [];
    var radio = document.getElementsByClassName('position');
    var k=0;
    for (var l=0; l<radio.length; l++){
        if (radio[l].checked){
          position[k] = radio[l].value;
          k++;
        }
    }*/

/*
    var c = document.getElementById('country');
    var country = c.options[c.selectedIndex].text;
    var p = document.getElementById('league');
    var league = p.options[p.selectedIndex].text;
*/

    var country = $('#country').val();
    var league = $('#league').val();
    var team = $('#team').val();
    var description = $('#description').val();

    var v_name = /^[a-zA-Z]{2,30}$/;
    //var val_dates = /^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d$/;
    var v_dates = /\d{2}.\d{2}.\d{4}$/;
    var v_surname = /^[0-9a-zA-Z]{2,20}$/;
    var v_appearances = /^[0-9]+$/;
    var string_description = /^(.){1,500}$/;

    $(".error").remove();
    if ($("#name").val() === "" || $("#name").val() === "Input player name"){
      $("#name").focus().after("<span class='error'>Input player name</span>");
      return false;
    }else if(!v_name.test($("#name").val())){
      $("#name").focus().after("<span class='error'>Name must be 2 to 30 letters</span>");
      return false;
    }

    if ($("#surname").val() === "" || $("#surname").val() === "Input player surname") {
        $("#surname").focus().after("<span class='error'>Input player surname</span>");
        return false;
    } else if (!v_surname.test($("#surname").val())) {
        $("#surname").focus().after("<span class='error'>Surname must be 2 to 30 letters</span>");
        return false;
    }

    if ($("#appearances").val() === "" || $("#appearances").val() === "Input the appearances") {
        $("#appearances").focus().after("<span class='error'>Input player appearances</span>");
        return false;
    } else if (!v_appearances.test($("#appearances").val())) {
        $("#appearances").focus().after("<span class='error'>Only possitive numbers admitted</span>");
        return false;
    }

    if ($("#date_reception").val() === "" || $("#date_reception").val() === "Input reception date") {
        $("#date_reception").focus().after("<span class='error'>JS Input player reception date</span>");
        return false;
    } else if (!v_dates.test($("#date_reception").val())) {
        $("#date_reception").focus().after("<span class='error'>JS Input player reception date</span>");
        return false;
    }

    if ($("#date_expiration").val() === "" || $("#date_expiration").val() === "Input expiration date") {
        $("#date_expiration").focus().after("<span class='error'>JS Input player expiration date</span>");
        return false;
    } else if (!v_dates.test($("#date_expiration").val())) {
        $("#date_expiration").focus().after("<span class='error'>JS Input player expiration date</span>");
        return false;
    }

    if ($("#country").val() === "" || $("#country").val() === "Select country" || $("#country").val() === null) {
        $("#country").focus().after("<span class='error'>Select one country</span>");
        return false;
    }

    if ($("#league").val() === "" || $("#league").val() === "Select league") {
        $("#league").focus().after("<span class='error'>Select one league</span>");
        return false;
    }

    if ($("#team").val() === "" || $("#team").val() === "Select team") {
        $("#team").focus().after("<span class='error'>Select one team</span>");
        return false;
    }

    if ($("#description").val() === "" || $("#description").val() === "Input player description") {
        $("#description").focus().after("<span class='error'>Input player description</span>");
        return false;
    } else if (!string_description.test($("#description").val())) {
        $("#description").focus().after("<span class='error'>Description cannot be empty</span>");
        return false;
    }

    //console.log("Before if result");
    if (result){
      //console.log("Inside if result");

        if (league === null) {
            league = 'default_league';
        }else if (league.length === 0) {
            league = 'default_league';
        }else if (league === 'Select league') {
            return 'default_league';
        }

        if (team === null) {
            team = 'default_team';
        }else if (team.length === 0) {
            team = 'default_team';
        }else if (team === 'Select team') {
            return 'default_team';
        }

      var data = {"name": name, "surname": surname, "position": position, "foot": foot, "country": country, "league": league, "team": team, "date_reception": date_reception, "date_expiration": date_expiration, "appearances": appearances, "description": description};
      // "country": country, "league": league, "team": team,
      // console.log(data); //Apleguen les dades be
      var data_players_JSON = JSON.stringify(data);
      console.log(data_players_JSON);
      $.post('../../players/alta_players/',
      // 'modules/players/controller/controller_players.class.php'
          {alta_players_json: data_players_JSON},
      function (response){
        console.log(response);
        if(response.success){
          // console.log(response.success);
          window.location.href = response.redirect;
        }
    },"json").fail(function(xhr, textStatus, errorThrown){
          //console.log("Inside error json");
          console.log(xhr);
          console.log(xhr.responseText);
          // console.log(xhr.responseJSON);
          // console.log(xhr.textstatus);
          if (xhr.status === 0) {
                alert('Not connect: Verify Network.');
            } else if (xhr.status == 404) {
                alert('Requested page not found [404]');
            } else if (xhr.status == 500) {
                alert('Internal Server Error [500].');
            } else if (textStatus === 'parsererror') {
                alert('Requested JSON parse failed.');
            } else if (textStatus === 'timeout') {
                alert('Time out error.');
            } else if (textStatus === 'abort') {
                alert('Ajax request aborted.');
            } else {
                alert('Uncaught Error: ' + xhr.responseText);
            }
          if (xhr.responseJSON == 'undefined' && xhr.responseJSON === null )
                  xhr.responseJSON = JSON.parse(xhr.responseText);

          if(xhr.responseJSON.error.name)
            $("#error_name").focus().after("<span  class='error1'>" + xhr.responseJSON.error.name + "</span>");

          if(xhr.responseJSON.error.surname)
            $("#error_surname").focus().after("<span  class='error1'>" + xhr.responseJSON.error.surname + "</span>");

          if(xhr.responseJSON.error.appearances)
            $("#error_appearances").focus().after("<span  class='error1'>" + xhr.responseJSON.error.appearances + "</span>");

          if(xhr.responseJSON.error.date_reception)
            $("#error_date_reception").focus().after("<span  class='error1'>" + xhr.responseJSON.error.date_reception + "</span>");

          if(xhr.responseJSON.error.date_expiration)
            $("#error_date_expiration").focus().after("<span  class='error1'>" + xhr.responseJSON.error.date_expiration + "</span>");

          if(xhr.responseJSON.error.position)
            $("#error_position").focus().after("<span  class='error1'>" + xhr.responseJSON.error.position + "</span>");

          if(xhr.responseJSON.error.country)
            $("#error_country").focus().after("<span  class='error1'>" + xhr.responseJSON.error.country + "</span>");

          if(xhr.responseJSON.error.league)
            $("#error_league").focus().after("<span  class='error1'>" + xhr.responseJSON.error.league + "</span>");

          if(xhr.responseJSON.error.team)
            $("#error_team").focus().after("<span  class='error1'>" + xhr.responseJSON.error.team + "</span>");

          if(xhr.responseJSON.error.description)
            $("#error_description").focus().after("<span  class='error1'>" + xhr.responseJSON.error.description + "</span>");

          if(xhr.responseJSON.error.img)
            $("#img").focus().after("<span  class='error1'>" + xhr.responseJSON.error.img + "</span>");

          if (xhr.responseJSON.success1) {
                if (xhr.responseJSON.img_avatar !== "/enno/media/default-avatar.png") {
                    $("#progress").show();
                    $("#bar").width('100%');
                    $("#percent").html('100%');
                    $('.msg').text('').removeClass('msg_error');
                    $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({ 'right' : '300px' }, 300);
                }
            } else {
                $("#progress").hide();
                $('.msg').text('').removeClass('msg_ok');
                $('.msg').text('Error Upload image!!').addClass('msg_error').animate({'right': '300px'}, 300);
            }

    });//End fail function hrx
    }//End if result
  }//End validate_player

Dropzone.autoDiscover = false;
$(document).ready(function () {
  $( "#date_reception" ).datepicker({
      dateFormat: 'dd/mm/yy',
      //dateFormat: 'mm-dd-yy',
      changeMonth: true, changeYear: true,
      minDate: -90, maxDate: "+1M"
  });
  $( "#date_expiration" ).datepicker({
    dateFormat: 'dd/mm/yy',
    //dateFormat: 'mm-dd-yy',
    changeMonth: true, changeYear: true,
    minDate: 0, maxDate: "+36M"
  });

  $('#players').click(function(){
      //console.log("Inside click function");
      //console.log($('input[name="packaging"]:checked').val());
      validate_player();
  });

  $("#dropzone").dropzone({
      url: "../../players/upload_players/",
      // url: "modules/players/controller/controller_players.class.php?upload=true",
      addRemoveLinks: true,
      maxFileSize: 1000,
      dictResponseError: "An error has occurred on the server",
      acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
      init: function () {
          this.on("success", function (file, response) {
              //alert(response);
              $("#progress").show();
              $("#bar").width('100%');
              $("#percent").html('100%');
              $('.msg').text('').removeClass('msg_error');
              $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
              console.log(file.name);
              console.log("Response: "+ response);
          });
      },
      complete: function (file) {
          //if(file.status == "success"){
          //alert("El archivo se ha subido correctamente: " + file.name);
          //}
      },
      error: function (file) {
          //alert("Error subiendo el archivo " + file.name);
      },
      removedfile: function (file, serverFileName) {
          var name = file.name;
          // console.log(name);
          $.ajax({
              type: "POST",
              url: "../../players/delete_players/",
              // url: "modules/players/controller/controller_players.class.php?delete=true",
              data: {"filename":name,"delete":true},
              success: function (data) {
                // console.log(name);
                // console.log(data);
                  $("#progress").hide();
                  $('.msg').text('').removeClass('msg_ok');
                  $('.msg').text('').removeClass('msg_error');
                  $("#e_avatar").html("");

                  var json = JSON.parse(data);
                  // console.log(json);
                  if (json.res === true) {
                      var element;
                      if ((element = file.previewElement) !== null) {
                          element.parentNode.removeChild(file.previewElement);
                          // alert("Imagen eliminada: " + name);
                      } else {
                          return false;
                      }
                  } else { //json.res == false, elimino la imagen también
                      var element2;
                      if ((element2 = file.previewElement) !== null) {
                          element2.parentNode.removeChild(file.previewElement);
                      } else {
                          return false;
                      }
                  }

              }
          });
      }
  });//End dropzone
});//End document ready

  var leagues;
  var countries;
  var teams;

  var searchIntoJson = function (obj, column, value) {
      var results = [];
      var valueField;
      var searchField = column;
      for (var i = 0 ; i < obj.length ; i++) {
          valueField = obj[i][searchField].toString();
          if (valueField === value) {
              results.push(obj[i]);
          }
      }
      return results;
  };

  var loadCountries = function () {
      $("#country").empty();
      $("#country").append('<option value="Select a Country" selected="selected"></option>');
      $.each(countries, function (i, valor) {
          $("#country").append("<option value='" + valor.country_id + "'>" + valor.country_name + "</option>");
      });
  };

  var loadLeagues = function (country_id) {
      var leaguesDepto = searchIntoJson(leagues, "country_id", country_id);
      $("#league").empty();
      $("#league").append('<option value="Select a League" selected="selected"></option>');
      $.each(leaguesDepto, function (i, valor) {
          $("#league").append('<option value="' + valor.leagueId + '">' + valor.league + '</option>');
      });
  };

  var loadTeams = function (leagueId) {
      var TeamsDepto = searchIntoJson(teams, "leagueId", leagueId);
      $("#team").empty();
      $("#team").append('<option value="Select a Team" selected="selected"></option>');
      $.each(TeamsDepto, function (i, valor) {
          $("#team").append('<option value="' + valor.teamId + '">' + valor.team + '</option>');
      });
  };

  $(document).ready(function () {
      $.getJSON("../../resources/countries.json", function (data) {
          countries = data;
      });

      $.getJSON("../../resources/leagues.json", function (data) {
          leagues = data;
          setTimeout(function () {
              if (leagues !== undefined) {
                  loadCountries();
              }
          }, 2000);
      });

      $.getJSON("../../resources/teams.json", function (data) {
          teams = data;
          setTimeout(function () {
              if (teams !== undefined) {
                  loadTeams();
              }
          }, 2000);
      });

      $("#country").change(function () {
          var country_id = $("#country").val();
          loadLeagues(country_id);
      });

      $("#league").change(function () {
          var leagueId = $("#league").val();
          loadTeams(leagueId);
      });
  });

  // function load_countries_v2(cad) {
  //     $.getJSON( cad, function(data) {
  //       $("#country").empty();
  //       $("#country").append('<option value="" selected="selected">Select country</option>');
  //
  //       $.each(data, function (i, valor) {
  //         $("#country").append("<option value='" + valor.country_id + "'>" + valor.country_name + "</option>");
  //       });
  //     })
  //     .fail(function() {
  //         alert( "error load_countries" );
  //     });
  // }
  //
  // function load_countries_v1() {
  //     $.get( "modules/players/controller/controller_players.class.php?load_country=true",
  //         function( response ) {
  //             //console.log(response);
  //             if(response === 'error'){
  //                 load_countries_v2("resources/countries.json");
  //             }else{
  //                 load_countries_v2("modules/players/controller/controller_players.class.php?load_country=true"); //oorsprong.org
  //             }
  //     })
  //     .fail(function(response) {
  //         load_countries_v2("resources/countries.json");
  //     });
  // }

  // function load_leagues_v2() {
  //   $.getJSON( cad, function(data) {
  //     $("#country").empty();
  //     $("#country").append('<option value="" selected="selected">Select country</option>');
  //
  //     $.each(data, function (i, valor) {
  //       $("#country").append("<option value='" + valor.leagueId + "'>" + valor.league + "</option>");
  //     });
  //   })
  //     // $.get("resources/provinciasypoblaciones.xml", function (xml) {
  // 	  //   $("#league").empty();
  // 	  //   $("#league").append('<option value="" selected="selected">Select league</option>');
  //     //
  //     //     $(xml).find("provincia").each(function () {
  //     //         var id = $(this).attr('id');
  //     //         var name = $(this).find('nombre').text();
  //     //         $("#league").append("<option value='" + id + "'>" + name + "</option>");
  //     //     });
  //     // })
  //     .fail(function() {
  //         alert( "error load_leagues" );
  //     });
  // }
  //
  // function load_leagues_v1() { //provinciasypoblaciones.xml - xpath
  //     $.get( "modules/players/controller/controller_players.class.php?load_leagues=true",
  //         function( response ) {
  //           $("#league").empty();
  // 	        $("#league").append('<option value="" selected="selected">Select league</option>');
  //
  //             //alert(response);
  //         var json = JSON.parse(response);
  // 		    var leagues=json.leagues;
  // 		    //alert(leagues);
  // 		    //console.log(leagues);
  //
  // 		    //alert(leagues[0].id);
  // 		    //alert(leagues[0].nombre);
  //
  //             if(leagues === 'error'){
  //                 load_leagues_v2();
  //             }else{
  //                 for (var i = 0; i < leagues.length; i++) {
  //         		    $("#league").append("<option value='" + leagues[i].id + "'>" + leagues[i].nombre + "</option>");
  //     		    }
  //             }
  //     })
  //     .fail(function(response) {
  //         load_leagues_v2();
  //     });
  // }
  //
  // function load_teams_v2(prov) {
  //     $.get("resources/provinciasypoblaciones.xml", function (xml) {
  // 		$("#team").empty();
  // 	    $("#team").append('<option value="" selected="selected">Select team</option>');
  //
  // 		$(xml).find('provincia[id=' + prov + ']').each(function(){
  //     		$(this).find('localidad').each(function(){
  //     			 $("#team").append("<option value='" + $(this).text() + "'>" + $(this).text() + "</option>");
  //     		});
  //         });
  // 	})
  // 	.fail(function() {
  //         alert( "error load_teams" );
  //     });
  // }
  //
  // function load_teams_v1(prov) { //provinciasypoblaciones.xml - xpath
  //     var datos = { idPoblac : prov  };
  // 	$.post("modules/players/controller/controller_products.class.php", datos, function(response) {
  // 	    //alert(response);
  //         var json = JSON.parse(response);
  // 		var teams=json.teams;
  // 		//alert(poblaciones);
  // 		//console.log(poblaciones);
  // 		//alert(poblaciones[0].poblacion);
  //
  // 		$("#team").empty();
  // 	    $("#team").append('<option value="" selected="selected">Select team</option>');
  //
  //         if(teams === 'error'){
  //             load_teams_v2(prov);
  //         }else{
  //             for (var i = 0; i < teams.length; i++) {
  //         		$("#team").append("<option value='" + teams[i].poblacion + "'>" + teams[i].poblacion + "</option>");
  //     		}
  //         }
  // 	})
  // 	.fail(function() {
  //         load_teams_v2(prov);
  //     });
  // }
