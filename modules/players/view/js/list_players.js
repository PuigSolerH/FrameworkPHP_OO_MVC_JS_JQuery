////////////////////////////////////////////////////////////////
function load_players_ajax() {
  var jqxhr = $.post("../players/load_players/",{'load':true}, function (data) {
    // console.log(data);
      var json = JSON.parse(data);
      // console.log(json);
      print_player(json);
      // alert( "success" );
  }).done(function () {
      // alert( "second success" );
  }).fail(function () {
      // alert( "error" );
  }).always(function () {
      // alert( "finished" );
  });
  jqxhr.always(function () {
      // alert( "second finished" );
  });
}

$(document).ready(function () {
  // alert("ACI ENTRE");
    load_players_ajax();
});

function print_player(data){
  // console.log(data);
  // console.log(data.player.img);
  $('#content').append("<div class='container'>"+
                        "<p>Name: " + data.player.name + "</p>" +
                        "<p>Surname: " + data.player.surname + "</p>" +
                        "<div class='icon'>" +
                          "<img src="+ data.player.img +"></img>" +
                        "</div>" +
                        "<p>Position: " + data.player.position + "</p>" +
                        "<p>Strong Foot: " + data.player.foot + "</p>" +
                        "<p>Country: " + data.player.country + "</p>" +
                        "<p>League: " + data.player.league + "</p>" +
                        "<p>Team: " + data.player.team + "</p>" +
                        "<p>Date: " + data.player.date_reception + "</p>" +
                        "<p>Date: " + data.player.date_expiration + "</p>" +
                        "<p>Appearances: " + data.player.appearances + "</p>" +
                        "<p>Description: " + data.player.description + "</p>" +
                      "</div>");
}
