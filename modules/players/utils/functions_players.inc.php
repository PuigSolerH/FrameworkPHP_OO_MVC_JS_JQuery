<?php

function validate_players($value){
  //echo json_encode("Inside validate_products on function products inc php");
//  exit;
    $error = array();
    $valid = true;
    $filter = array(
        'name' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[A-Za-z\s]{2,30}$/')
        ),
        'surname' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[A-Za-z]{2,30}$/')
        ),
        // 'country' => array(
        //     'filter' => FILTER_VALIDATE_REGEXP,
        //     'options' => array('regexp' => '/^[a-zA-Z_]*$/')
        // ),
        // 'league' => array(
        //     'filter' => FILTER_VALIDATE_REGEXP,
        //     'options' => array('regexp' => '/^[a-zA-Z0-9, _]*$/')
        // ),
        // 'team' => array(
        //     'filter' => FILTER_CALLBACK,
        //     'options' => 'validate_team'
        // ),
        'appearances' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9]{1,30}$/')
        ),
        'date_reception' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/')
        ),
        'date_expiration' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/')
        ),
        'description' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(.){1,500}$/')
        )
    );

    $result = filter_var_array($value, $filter);

    //Non checked data:
    $result['foot'] = $value['foot'];
    $result['position'] = $value['position'];
    $result['country'] = $value['country'];
    $result['league'] = $value['league'];
    $result['team'] = $value['team'];

    if (count($result['foot']) < 1 ){
        $error['foot'] = "PHP Select 1 strong foot or more";
        $valid = false;
    }

    if($result['date_reception'] && $result['date_expiration']){
        $dates = validate_dates3($result['date_reception'],$result['date_expiration']);
        if($dates){
            $error['date_reception'] = 'PHP Reception date must be before of the expiration date';
            $error['date_expiration'] = 'PHP Expiration date must be after reception date';
            $valid = false;
        }
    }

    if($result['name']==='PHP Input player name'){
            $error['name']="PHP Name must be 2 to 30 letters";
            $valid = false;
    }

    // if ($result['country']) {
    //     $error['country'] = 'Select correct country';
    //     $resultado['country'] = $value['country'];
    //     $valid = false;
    // }
    // if ($result['league']) {
    //     $error['league'] = 'Select correct league';
    //     $resultado['league'] = $value['league'];
    //     $valid = false;
    // }
    // if ($result['team']) {
    //     $error['team'] = 'Select correct team';
    //     $resultado['team'] = $value['team'];
    //     $valid = false;
    // }

    if ($result['description']==='PHP Input player description'){
            $error['description']="PHP Please enter a description";
            $valid = false;
    }

    if ($result != null && $result){
        if(!$result['name']){
            $error['name'] = "PHP Name must be 2 to 30 letters";
            $valid = false;
        }

        if(!$result['surname']){
            $error['surname'] = "PHP Surname must be 2 to 30 letters";
            $valid = false;
        }

        if(!$result['appearances']){
            $error['appearances'] = "PHP Appearances must be numbers (like 1)";
            $valid = false;
        }

        if (!$result['date_reception']) {
            if ($result['date_reception'] == "") {
                $error['date_reception'] = " PHP Reception date can't be empty";
                $valid = false;
            } else {
                $error['date_reception'] = 'PHP error reception format date (dd/mm/yyyy)';
                $valid = false;
            }
        }

        if (!$result['date_expiration']) {
            if ($result['date_expiration'] == "") {
                $error['date_expiration'] = "PHP Expiration date can't be empty";
                $valid = false;
            } else {
                $error['date_expiration'] = 'PHP error format date (dd/mm/yyyy)';
                $valid = false;
            }
        }

        if(!$result['description']){
            $error['description'] = "PHP Description must be 2 to 90 letters";
            $valid = false;
        }
    } else {
        $valid = false;
    };

    return $return = array('result' => $valid, 'error' => $error, 'data' => $result );
}//End of function validate product


//http://stackoverflow.com/questions/8722806/how-to-compare-two-dates-in-php

//http://php.net/manual/es/datetime.diff.php

function val_dates($datetime1,$datetime2){
    $date1 = new DateTime();
    $newDate1 = $date1->createFromFormat('d/m/Y', $datetime1);
    $date2 = new DateTime();
    $newDate2 = $date2->createFromFormat('d/m/Y', $datetime2);
    var_dump($newDate1->diff($newDate2));

    if($date1 <= $date2){
      return true;
    }
      return false;
 }

function validate_dates($start_days, $dayslight) {
    $start_day = date("m/d/Y", strtotime($start_days));
    $daylight = date("m/d/Y", strtotime($dayslight));

    list($mes_one, $dia_one, $anio_one) = split('/', $start_day);
    list($mes_two, $dia_two, $anio_two) = split('/', $daylight);

    $dateOne = new DateTime($anio_one . "-" . $mes_one . "-" . $dia_one);
    $dateTwo = new DateTime($anio_two . "-" . $mes_two . "-" . $dia_two);

    if ($dateOne <= $dateTwo) {
        return true;
    }
    return false;
}

function validate_dates2($first_date,$second_date){
      $day1 = substr($first_date, 0, 2);
      $month1 = substr($first_date, 3, 2);
      $year1 = substr($first_date, 6, 4);
      $day2 = substr($second_date, 0, 2);
      $month2 = substr($second_date, 3, 2);
      $year2 = substr($second_date, 6, 4);
      //echo json_encode(strtotime($day2 . "-" . $month2 . "-" . $year2);
      //exit;

      if(strtotime($day1 . "-" . $month1 . "-" . $year1) <= strtotime($day2 . "-" . $month2 . "-" . $year2)){
        return true;
      }
        return false;
}

function validate_dates3($date1,$date2){
  $fixedDate = $date1;
  $variableDate = $date2;
  // Now we do our timestamping magic!
  $fixedDate = implode('', array_reverse(explode('/', $fixedDate)));
  $variableDate = implode('', array_reverse(explode('/', $variableDate)));

    if ($variableDate < $fixedDate){ // 20100428 < 20090501
        return true;
    }
        return false;
}

function check_in_range($start_date, $end_date){
      // Convert to timestamp
      $start_ts = strtotime($start_date);
      $end_ts = strtotime($end_date);

      // Check that user date is between start & end
      if ($start_ts <= $end_ts){
        return true;
      }else{
        return false;
      }
}

// function validate_town($town) {
//     $town = filter_var($town, FILTER_SANITIZE_STRING);
//     return $town;
// }
