function validate_search(search_value) {
    if (search_value.length > 0) {
        var regexp = /^[a-zA-Z0-9 .,]*$/;
        return regexp.test(search_value);
    }
    return false;
}

// function refresh() {
//     $('.pagination').html = '';
//     $('.pagination').val = '';
// }

function search_not_empty(keyword) {
  // alert("search_not_empty");
    //console.log("data");
    ////////////////////////// keyword.length >= 1 /////////////////////////
    //"index.php?module=home&function=num_pages_players&num_pages=true&keyword="
    $.post("../../home/num_pages_players/", {'num_pages': true, 'keyword': keyword}, function (data, status) {
      // alert("num_pages_players search_not_empty");
        // console.log(data);
        var json = JSON.parse(data);
        var pages = json.pages;
        var count = 0;

        //"index.php?module=home&function=obtain_players&keyword=" + keyword
        $("#results").load("../../home/obtain_players/", {'keyword': keyword});

        if (pages !== 0) {
            $('.pagination').val('');
            $(".pagination").bootpag({
                total: pages,
                page: 1,
                maxVisible: 3,
                next: 'next',
                prev: 'prev'
            }).on("page", function (e, num) {
                e.preventDefault();
                //"index.php?module=home&function=obtain_players"
                $("#results").load("../../home/obtain_players/", {'page_num': num, 'keyword': keyword});
                reset();
            });
        } else {
            //"index.php?module=home&function=view_error_false&view_error=false"
            $("#results").load("../../home/view_error_false/", {'view_error': false}); //view_error=false
            $('.pagination').html('');
            reset();
        }
        reset();
    }).fail(function (xhr) {
        //"index.php?module=home&function=view_error_true&view_error=true"
        $("#results").load("../../home/view_error_true/", {'view_error': true});
        $('.pagination').html('');
        reset();
    });
}

function search_empty() {
  // alert("search_empty");
    ////////////////////////// keyword.length == 0 /////////////////////////
    //index.php?module=home&function=num_pages_players&num_pages=true
    $.post("../../home/num_pages_players/", {'num_pages': true}, function (data, status) {
      // alert("num_pages_players search_empty");
        // console.log(data);
        var json = JSON.parse(data);
        // console.log(json);
        var pages = json.pages;
        // console.log(pages);
        $('#results').html('');
        //"index.php?module=home&function=obtain_players"
        $("#results").load("../../home/obtain_players/"); //load initial records

        $(".pagination").bootpag({
            total: pages,
            page: 1,
            maxVisible: 3,
            next: 'next',
            prev: 'prev'
        }).on("page", function (e, num) {
            // console.log(num);
            e.preventDefault();
            $("#results").load("../../home/obtain_players/", {'page_num': num});
            reset();
        });
        reset();
    }).fail(function (xhr) {
      // alert("FAIL");
        //"index.php?module=players&function=view_error_true&view_error=true"
        $("#results").load("../../home/view_error_true/", {'view_error': true});
        $('.pagination').html('');
        reset();
    });
}

function search_player(keyword) {
  // alert("search_player");

  // $.get("modules/home/controller/controller_home.class.php?nom_player=" + keyword, function (data, status) {

    $.post("../../home/name_player/",{'name_player': keyword}, function (data, status) {
      // alert("name_player search_player");
        // console.log(data);
        var json = JSON.parse(data);
        var player = json.player_autocomplete;
        console.log(player);

        $('#results').html('');
        $('.pagination').html('');

        $('#details_player').empty();
        $('#details_player').append('<div id="details">' +
                                '<div id="img" class="prodImg">' +
                                  '<img src="' + player.img + '" height="75" width="75"> ' +
                                '</div>' +
                                '<div id="container">' +
                                  '<h4><strong><div id="name">' + player.name + '</div></strong></h4>' +
                                  '<p>' +
                                    '<div id="description">' + player.description + '</div>' +
                                    '<div id="appearances">' + player.appearances + '</div>' +
                                  '</p>' +
                                '</div>' +
                              '</div>');

        //we do this so that  details_player  appear
        $("#details_player").show();

    }).fail(function (xhr) {
        $("#results").load("../../home/view_error_false/", {'view_error': false});
        $('.pagination').html('');
        reset();
    });
}

function count_players(keyword) {
  // alert("count_players");
  // $.get("modules/home/controller/controller_home.class.php?count_players=" + keyword, function (data, status) {

    $.post("../../home/count_players/", {'count_players': keyword}, function (data, status) {
      // alert("count_players count_players");
      // console.log(status);
      console.log(data);

        var json = JSON.parse(data);
        var num_players = json.num_players;
        alert("num_players: " + num_players);

        if (num_players == 0) {
            $("#results").load("../../home/view_error_false/", {'view_error': false});
            $('.pagination_prods').html('');
            reset();
        }
        if (num_players == 1) {
            search_player(keyword);
        }
        if (num_players > 1) {
            search_not_empty(keyword);
        }
    }).fail(function () {
        $("#results").load("../../home/view_error_false/", {'view_error': false});  //view_error=false
        $('.pagination_prods').html('');
        reset();
    });
}

function reset() {
    $('#details_player').html('');
    $('#keyword').val('');
}

$(document).ready(function () {
  // alert("document ready");
    ////////////////////////// inici carregar pàgina /////////////////////////
    if (!getCookie('keyword')) {
        //console.log("good");
        search_empty();
    } else {
        //console.log("hola");
        $('#keyword').val((getCookie('keyword')));
        count_players(getCookie('keyword'));
        setCookie('keyword', '', 1);
    }

    $("#search_prod").submit(function (e) {
      var keyword = '';
      keyword = $('#keyword').val();
      var v_keyword = validate_search(keyword);

      if (!v_keyword) {
          search_empty();
      } else {
          setCookie('keyword', keyword, 1);
      }
      //reboot page becase  we  have problem with bootpag plugin
      location.reload(true);
      //si no ponemos la siguiente línea, el navegador nos redirecciona a index.php
      e.preventDefault(); //STOP default action
    });

    $('#Submit').click(function () {
        var keyword = document.getElementById('keyword').value;
        var v_keyword = validate_search(keyword);
        if (v_keyword)
            setCookie('keyword', keyword, 1);
        // alert("getCookie(search): " + getCookie("search"));
        location.reload(true);
    });

    // $.get("modules/home/controller/controller_home.class.php?autocomplete=true", function (data, status) {
    $.post("../../home/autocomplete_players/", {'autocomplete': true}, function (data, status) {
      // alert("autocomplete document_ready");
        // console.log(status);
        // console.log(data);
        var json = JSON.parse(data);
        var name_players = json.name_players;
        //alert(name_players[0].nombre);
        // console.log(name_players);

        var suggestions = new Array();
        for (var i = 0; i < name_players.length; i++) {
            suggestions.push(name_players[i].name);
        }
        //alert(suggestions);
        // console.log(suggestions);

        $("#keyword").autocomplete({
            source: suggestions,
            minLength: 1,
            select: function (event, ui) {
                //alert(ui.item.label);
                var keyword = ui.item.label;
                count_players(keyword);
            }
        });
    }).fail(function (xhr) {
      if (xhr.status === 404) {
          $("#results").load("../../home/view_error_false/", {'view_error': false});
      } else {
          $("#results").load("../../home/view_error_true/", {'view_error': true});
      }
    });
});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return "";
}
