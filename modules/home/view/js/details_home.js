//we do this so that  details_prod don't appear
$("#details_player").hide();
$(document).ready(function () {
    $('.player_name').click(function () {
        var id = this.getAttribute('id');
        // alert(id);
        //"index.php?module=home&function=id&id=" + id
        $.post("../../home/id/", {'id': id}, function (data, status) {
            // console.log(id);
            var json = JSON.parse(data);
            var player = json.player;
            // console.log(player);
              $('#details_player').empty();
              $('#details_player').append('<div id="details">' +
                                      '<div id="img" class="prodImg">' +
                                        '<img src="' + player.img + '" height="75" width="75"> ' +
                                      '</div>' +
                                      '<div id="container">' +
                                        '<h4><strong><div id="name">' + player.name + '</div></strong></h4>' +
                                        '<p>' +
                                          '<div id="description">' + player.description + '</div>' +
                                          '<div id="appearances">' + player.appearances + '</div>' +
                                        '</p>' +
                                      '</div>' +
                                    '</div>');
            $('#results').html('');
            $('.pagination').html('');

            $("#details_player").show();
        })
                .fail(function (xhr) {
                    ///if  we already have an error 404
                    if (xhr.status === 404) {
                        $("#results").load("../../home/view_error_false/", {'view_error': false});
                    } else {
                        $("#results").load("../../home/view_error_true/", {'view_error': true});
                    }
                    $('.pagination').html('');
                    $('#keyword').val('');
                });
    });
});
