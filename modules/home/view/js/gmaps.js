$(document).ready(function () {
  //
  var map = new google.maps.Map(document.getElementById('map_canvas'), {
    center: {lat: 38.9047696, lng: -0.4251715},
    zoom: 10
  });

  var infoWindow = new google.maps.InfoWindow({map: map});
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };

      // infoWindow.setPosition(pos);
      // infoWindow.setContent('Location found');
      map.setCenter(pos);
    }, function() {
      handleLocationError(true, infoWindow, map.getCenter());
    });
  } else {
    // Browser doesn't support Geolocation
    handleLocationError(false, infoWindow, map.getCenter());
  }

  function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' : 'Error: Your browser doesn\'t support geolocation.');
  }

  $.post("../../home/gmaps/",{'gmaps': true}, function (data, status) {
      var json = JSON.parse(data);
      // console.log(json);

      for (var i = 0; i < json.g_maps.length; i++) {
          var lat = json.g_maps[i].lat;
          var lng = json.g_maps[i].lon;
          var name = json.g_maps[i].name;
          var surname = json.g_maps[i].surname;
          var position = json.g_maps[i].position;
          var img = json.g_maps[i].img;

          var html = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<h1 id="firstHeading" class="firstHeading">'+ name +' '+ surname +'</h1>'+
                '<div id="bodyContent">'+
                position + '</br>' +
                // '<a href="#" style="color:blue">MORE INFO</a>' +
                '</div>'+
                '</div>';

                  var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    title: name,
                    map: map
                  });
                  bindInfoWindow(marker, map, infoWindow, html);
      }
      function bindInfoWindow(marker, map, infoWindow, html) {
        google.maps.event.addListener(marker, 'click', function() {
          infoWindow.setContent(html);
          infoWindow.open(map, marker);
        });
      }
  });
});
