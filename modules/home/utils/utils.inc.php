<?php
function paint_template_error($message) {
    $arrData = response_code(http_response_code());

    print ("<div id='page'>");
    print ("<br><br>");
    print ("<div id='header' class='status4xx'>");
    print("<h1>" . $message . "</h1>");
    print("</div>");
    print ("<div id='content'>");
    print ("<h2>The following error occurred:</h2>");
    print ("<p>The requested URL was not found on this server.</p>");
    print ("<P>Please check the URL or contact the <!--WEBMASTER//-->webmaster<!--WEBMASTER//-->.</p>");
    print ("</div>");
    print ("<div id='footer'>");
    print ("<p>Powered by <a href='http://www.ispconfig.org'>ISPConfig</a></p>");
    print ("</div>");
    print("</div>");
}

function paint_template_players($arrData) {
    print ("<script type='text/javascript' src='".HOME_JS_PATH."details_home.js' ></script>");
    print('<section id="services" >');
    print('<div class="container">');
    print('<div class="table-display">');
    if (isset($arrData) && !empty($arrData)) {
        $i = 0;
        foreach ($arrData as $player) {
            $i++;
            if (count($arrData) % 2 !== 0 && $i >= count($arrData))
                print( '<div class="odd_prod">');
            else {
                if ($i % 2 != 0)
                    print( '<div class="table-row">');
                else
                    print('<div class="table-separator"></div>');
            }
            print('<div class="col-md-4">');
            print('<div class="wow bounceIn" data-wow-offset="0" data-wow-delay="0.4s">');
            print('<div class="icon">');
            print('<img src="' . $player['img'] . '" class="icon-md" height="80" width="80"></img>');
            print('</div>');
            print('<h3 class="media-heading">' . $player['name'] . '</h3>');
            print('<p>' . $player['description'] . '</p>');
            print('<h5> Position:' . $player['position'] . '</h5>');
            print("<button id='" . $player['id'] . "' class='player_name'>Read Details</button>");
            print('</div>');
            print('</div>');
            if (count($arrData) % 2 !== 0 && $i >= count($arrData))
                print( '</div>');
            else {
                if ($i % 2 == 0)
                    print('</div> <br>');
            }
        }
    }
    print('</div>');
    print('</div>');
    print('</section> ');
}

function paint_template_search($message) {
    print ("<section> \n");
    print ("<div class='container'> \n");
    print ("<div class='row text-center pad-row'> \n");
    print ("<h2>" . $message . "</h2> \n");
    print ("<br><br><br><br> \n");
    print ("</div> \n");
    print ("</div> \n");
    print ("</section> \n");
}
