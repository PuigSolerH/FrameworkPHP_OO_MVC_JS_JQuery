<?php
class controller_home {
    function __construct() {
        include(FUNCTIONS_HOME . "utils.inc.php");
        $_SESSION['module'] = "home";
    }

    function list_players() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView(HOME_VIEW_PATH, 'list_players.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }


    function autocomplete_players() {
      // echo json_encode("hola");
      // die();
        if ($_POST["autocomplete"]) {
            set_error_handler('ErrorHandler');
            try {
                $name_players = loadModel(MODEL_HOME, "home_model", "live_search");
                // echo json_encode("$name_players");
                // die();
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($name_players) {
                $jsondata["name_players"] = $name_players;
                echo json_encode($jsondata);
                exit;
            } else {
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

function name_player() {
    if (isset($_POST["name_player"])) {
        $result = filter_string($_POST["name_player"]);
        if ($result['resultado']) {
            $criteria = $result['datos'];
        } else {
            $criteria = '';
        }

        set_error_handler('ErrorHandler');
        try {
            $player = loadModel(MODEL_HOME, "home_model", "obtain_player", $criteria);
        } catch (Exception $e) {
            showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
        }
        restore_error_handler();

        if ($player[0]) {
            $jsondata["player_autocomplete"] = $player[0];
            echo json_encode($jsondata);
            exit;
        } else {
            showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
        }
    }
}

    function count_players() {
        if (isset($_POST["count_players"])) {
            $result = filter_string($_POST["count_players"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }

            set_error_handler('ErrorHandler');
            try {
                $result = loadModel(MODEL_HOME, "home_model", "total_rows_like", $criteria);
                $total_rows = $result[0]['total'];
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($total_rows) {
                $jsondata["num_players"] = $total_rows;
                echo json_encode($jsondata);
                exit;
            } else {
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    function num_pages_players() {
        //obtain num total pages
        if ($_POST["num_pages"]) {
            $item_per_page = 3;

            if (isset($_POST["keyword"])) {
                $result = filter_string($_POST["keyword"]);
                if ($result['resultado']) {
                    $criteria = $result['datos'];
                } else {
                    $criteria = '';
                }
            } else {
                $criteria = '';
            }

            set_error_handler('ErrorHandler');
            try {
                $arrValue = loadModel(MODEL_HOME, "home_model", "total_players", $criteria);
                $get_total_rows = $arrValue[0]["total"];
                $pages = ceil($get_total_rows / $item_per_page);
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($get_total_rows) {
                $jsondata["pages"] = $pages;
                echo json_encode($jsondata);
                exit;
            } else {
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    function view_error_true() {
        if ($_POST["view_error"]) {
            showErrorPage(0, "ERROR - 503 BD Unavailable");
        }
    }

    function view_error_false() {
        if ($_POST["view_error"]) {
            showErrorPage(3, "RESULTS NOT FOUND");
        }
    }

    function id() {
        if (isset($_POST["id"])) {
            $arrValue = null;

            $result = filter_num_int($_POST["id"]);
            if ($result['resultado']) {
                $id = $result['datos'];
            } else {
                $id = 1;
            }

            set_error_handler('ErrorHandler');
            try {
                $arrValue = loadModel(MODEL_HOME, "home_model", "details_players", $id);
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($arrValue) {
                $jsondata["player"] = $arrValue[0];
                echo json_encode($jsondata);
                exit;
            } else {
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    function obtain_players() {
        if (isset($_POST["page_num"])) {
            $result = filter_num_int($_POST["page_num"]);
            if ($result['resultado']) {
                $page_number = $result['datos'];
            }
        } else {
            $page_number = 1;
        }

        if (isset($_POST["keyword"])) {
            $result = filter_string($_POST["keyword"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }
        } else {
            $criteria = '';
        }

        if (isset($_POST["keyword"])) {
            $result = filter_string($_POST["keyword"]);
            if ($result['resultado']) {
                $criteria1 = $result['datos'];
            } else {
                $criteria1 = '';
            }
        } else {
            $criteria1 = '';
        }

        if (isset($_POST["keyword"])) {
            $criteria = $criteria1;
        }

        set_error_handler('ErrorHandler');
        try {
            $item_per_page = 3;
            $position = (($page_number - 1) * $item_per_page);

            $arrArgument = array(
                'position' => $position,
                'item_per_page' => $item_per_page,
                'criteria' => $criteria
            );
            $arrValue = loadModel(MODEL_HOME, "home_model", "page_players", $arrArgument);
        } catch (Exception $e) {
            showErrorPage(0, "ERROR - 503 BD Unavailable");
        }
        restore_error_handler();

        if ($arrValue) {
            paint_template_players($arrValue);
        } else {
            showErrorPage(0, "ERROR - 404 NO PRODUCTS");
        }
    }

    function gmaps() {
        if (isset($_POST["gmaps"])) {
            $result = filter_string($_POST["gmaps"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }

            set_error_handler('ErrorHandler');
            try {
                $arrValue = loadModel(MODEL_HOME, "home_model", "maps", $criteria);
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();
            if ($arrValue) {
                $jsondata["g_maps"] = $arrValue;
                echo json_encode($jsondata);
                exit;
            } else {
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }
}
