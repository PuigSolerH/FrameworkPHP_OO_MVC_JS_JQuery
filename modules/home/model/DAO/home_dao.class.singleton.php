<?php
class home_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_players_DAO($db) {
        $sql = "SELECT * FROM players";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function details_players_DAO($db, $id) {
        $sql = "SELECT * FROM players WHERE id=" . $id;
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function page_players_DAO($db, $arrArgument) {
        $position = $arrArgument['position'];
        $item_per_page = $arrArgument['item_per_page'];
        $criteria = $arrArgument['criteria'];

        $sql = "SELECT DISTINCT * FROM players WHERE name like '%" . $criteria . "%' ORDER BY id ASC LIMIT " . $position . ", " . $item_per_page;

        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function total_players_DAO($db, $criteria) {
        $sql = "SELECT COUNT(*) as total FROM players WHERE name like '%" . $criteria . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function live_search_DAO($db) {
        $sql = "SELECT name FROM players ORDER BY name";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function obtain_player_DAO($db, $criteria) {
        $sql = "SELECT DISTINCT * FROM players WHERE name like '%" . $criteria . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function total_rows_like_DAO($db, $criteria) {
        $sql = "SELECT COUNT(*) as total FROM players WHERE name like '%" . $criteria . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function maps_DAO($db,$criteria){
      $sql = "SELECT * FROM players";
      $stmt = $db->ejecutar($sql);
      return $db->listar($stmt);
    }
}
