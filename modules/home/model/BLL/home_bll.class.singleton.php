<?php
class home_bll {
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = home_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_players_BLL() {
        return $this->dao->list_players_DAO($this->db);
    }

    public function details_players_BLL($id) {
        return $this->dao->details_players_DAO($this->db, $id);
    }

    public function page_players_BLL($arrArgument) {
        return $this->dao->page_players_DAO($this->db, $arrArgument);
    }

    public function total_players_BLL($criteria) {
        return $this->dao->total_players_DAO($this->db, $criteria);
    }

    public function live_search_BLL() {
        return $this->dao->live_search_DAO($this->db);
    }

    public function obtain_player_BLL($criteria) {
        return $this->dao->obtain_player_DAO($this->db, $criteria);
    }

    public function total_rows_like_BLL($criteria) {
        return $this->dao->total_rows_like_DAO($this->db, $criteria);
    }

    public function maps_BLL($criteria) {
        return $this->dao->maps_DAO($this->db, $criteria);
    }
}
