<?php
class home_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = home_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_players() {
        return $this->bll->list_players_BLL();
    }

    public function details_players($id) {
        return $this->bll->details_players_BLL($id);
    }

    public function page_players($arrArgument) {
        return $this->bll->page_players_BLL($arrArgument);
    }

    public function total_players($criteria) {
        return $this->bll->total_players_BLL($criteria);
    }

    public function live_search() {
        return $this->bll->live_search_BLL();
    }

    public function obtain_player($criteria) {
        return $this->bll->obtain_player_BLL($criteria);
    }

    public function total_rows_like($criteria) {
        return $this->bll->total_rows_like_BLL($criteria);
    }

    public function maps($criteria) {
        return $this->bll->maps_BLL($criteria);
    }
}
